#!/bin/sh

composer install --prefer-dist --no-progress --no-interaction

bin/console doctrine:database:create --if-not-exists
bin/console doctrine:migrations:migrate --no-interaction

exec "$@"
