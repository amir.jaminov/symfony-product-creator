<?php

declare(strict_types=1);

namespace App\Entity;

use App\DTO\ProductDto;
use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(min: 1, max: 255)]
    private string $name;

    #[ORM\Column(type: 'float')]
    #[Assert\NotNull]
    private float $price;

    #[ORM\Column(type: 'float')]
    #[Assert\NotNull]
    private float $VAT;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(min: 1, max: 255)]
    private string $category;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    private string $description;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(min: 0, max: 255)]
    private ?string $owner;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Assert\PositiveOrZero]
    private ?int $deliveryTime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getVAT(): ?float
    {
        return $this->VAT;
    }

    public function setVAT(float $VAT): self
    {
        $this->VAT = $VAT;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOwner(): ?string
    {
        return $this->owner;
    }

    public function setOwner(?string $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getDeliveryTime(): ?int
    {
        return $this->deliveryTime;
    }

    public function setDeliveryTime(?int $deliveryTime): self
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    public function dtoMapper(ProductDto $productDto): self
    {
        if (!is_null($productDto->getName())) {
            $this->setName($productDto->getName());
        }

        if (!is_null($productDto->getPrice())) {
            $this->setPrice($productDto->getPrice());
        }

        if (!is_null($productDto->getVAT())) {
            $this->setVAT($productDto->getVAT());
        }

        if (!is_null($productDto->getVAT())) {
            $this->setVAT($productDto->getVAT());
        }

        if (!is_null($productDto->getCategory())) {
            $this->setCategory($productDto->getCategory());
        }

        if (!is_null($productDto->getDescription())) {
            $this->setDescription($productDto->getDescription());
        }

        if (!is_null($productDto->getOwner())) {
            $this->setOwner($productDto->getOwner());
        }

        if (!is_null($productDto->getDeliveryTime())) {
            $this->setDeliveryTime($productDto->getDeliveryTime());
        }

        return $this;
    }
}
