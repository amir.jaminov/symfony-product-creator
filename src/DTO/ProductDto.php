<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ProductDto
{
    #[Assert\NotNull(groups: ['create'])]
    #[Assert\Length(min: 1, max: 255)]
    public ?string $name;

    #[Assert\NotNull(groups: ['create'])]
    #[Assert\PositiveOrZero]
    public ?float $price;

    #[Assert\NotNull(groups: ['create'])]
    #[Assert\PositiveOrZero]
    public ?float $VAT;

    #[Assert\NotNull(groups: ['create'])]
    #[Assert\Length(min: 1, max: 255)]
    public ?string $category;

    #[Assert\NotNull(groups: ['create'])]
    public ?string $description;

    #[Assert\Length(min: 0, max: 255)]
    public ?string $owner;

    #[Assert\PositiveOrZero]
    public ?int $deliveryTime;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getVAT(): ?float
    {
        return $this->VAT;
    }

    public function setVAT(float $VAT): void
    {
        $this->VAT = $VAT;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getOwner(): ?string
    {
        return $this->owner;
    }

    public function setOwner(string $owner): void
    {
        $this->owner = $owner;
    }

    public function getDeliveryTime(): ?int
    {
        return $this->deliveryTime;
    }

    public function setDeliveryTime(int $deliveryTime): void
    {
        $this->deliveryTime = $deliveryTime;
    }

    public static function creator(Request $request): self
    {
        $productDto = new self();
        $productDto->name = $request->request->get('name');
        $productDto->price = is_null($request->request->get('price'))
            ? null
            : (float) $request->request->get('price');
        $productDto->VAT = is_null($request->request->get('VAT'))
            ? null
            : (float) $request->request->get('VAT');
        $productDto->category = $request->request->get('category');
        $productDto->description = $request->request->get('description');
        $productDto->owner = $request->request->get('owner');
        $productDto->deliveryTime = is_null($request->request->get('deliveryTime'))
            ? null
            : (int) $request->request->get('deliveryTime');

        return $productDto;
    }
}
