<?php

declare(strict_types=1);

namespace App;

use JetBrains\PhpStorm\Pure;

class InvalidProductArgumentException extends \InvalidArgumentException
{
    private array $violationMessageList;

    #[Pure] public function __construct(
        array $violationMessageList = null
    ) {
        parent::__construct();

        $this->violationMessageList = $violationMessageList;
    }

    public function getViolationMessageList(): array
    {
        return $this->violationMessageList;
    }
}
