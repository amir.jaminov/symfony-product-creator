<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductGetter
{
    public function __construct(private ProductRepository $productRepository)
    {}

    public function getById(int $id): Product
    {
        $product = $this->productRepository->find($id);

        if (is_null($product)) {
            throw new NotFoundHttpException('Product not found');
        }

        return $product;
    }

    /** @return Product[] */
    public function getAll(?string $category = null): array
    {
        if (is_null($category)) {
            return $this->productRepository->findAll();
        }

        return $this->productRepository->findBy(['category' => $category]);
    }
}
