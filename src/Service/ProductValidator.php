<?php

declare(strict_types=1);

namespace App\Service;

use App\DTO\ProductDto;
use App\InvalidProductArgumentException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductValidator
{
    public function __construct(private ValidatorInterface $validator)
    {}

    public function validate(ProductDto $productDto, ?array $groups = null): void
    {
        $constraintViolationList = $this->validator->validate($productDto, null, $groups);

        if ($constraintViolationList->count() !== 0) {
            $violationMessageList = [];

            foreach ($constraintViolationList as $violation) {
                $violationMessageList[] = [
                    $violation->getPropertyPath() => $violation->getMessageTemplate(),
                ];
            }
            throw new InvalidProductArgumentException($violationMessageList);
        }
    }
}
