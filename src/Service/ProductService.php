<?php

declare(strict_types=1);

namespace App\Service;

use App\DTO\ProductDto;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductService
{
    public function __construct(
        private ProductRepository $productRepository,
        private ProductValidator $validator
    ) {
    }

    public function create(Request $request): Product
    {
        $productDto = ProductDto::creator($request);
        $productDto->VAT = $productDto->VAT ?? 20;

        $this->validator->validate($productDto, ['create']);

        $product = new Product();
        $product = $product->dtoMapper($productDto);

        $this->productRepository->save($product);

        return $product;
    }

    public function update(Request $request, int $id): void
    {
        $productDto = ProductDto::creator($request);

        $this->validator->validate($productDto);

        $product = $this->productRepository->find($id);

        if (is_null($product)) {
            throw new NotFoundHttpException('Product not found');
        }

        $product = $product->dtoMapper($productDto);

        $this->productRepository->save($product);
    }
}
