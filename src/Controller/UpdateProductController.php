<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\ProductDto;
use App\InvalidProductArgumentException;
use App\Service\ProductService;
use Exception;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UpdateProductController extends AbstractController
{
    #[Route('/products/update/{id}', name: 'update_product', methods: ['PUT'])]
    public function updateProduct(
        int $id, Request $request, ProductService $productService
    ): Response {
        try {
            $productService->update($request, $id);
        } catch (InvalidProductArgumentException $exception) {
            return $this->json(['error' => $exception->getViolationMessageList()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (NotFoundHttpException $exception) {
            return $this->json(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        } catch (Exception) {
            return $this->json(['error' => 'Internal server error'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
