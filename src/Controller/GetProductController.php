<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\ProductGetter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class GetProductController extends AbstractController
{
    #[Route('/products/{id}', name: 'get_product', methods: ['GET'])]
    public function getProduct(
        int $id, ProductGetter $productGetter, NormalizerInterface $normalizer
    ): Response {
        try {
            $product = $productGetter->getById($id);
        } catch (NotFoundHttpException $exception) {
            return $this->json(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }

        return $this->json([
            'product' => $normalizer->normalize($product, 'json', [
                AbstractNormalizer::ATTRIBUTES => [
                    'name',
                    'price',
                    'category',
                ],
            ])
        ]);
    }

    #[Route('/products/{id}/full', name: 'get_full_product', methods: ['GET'])]
    public function getFullProduct(
        int $id, ProductGetter $productGetter, NormalizerInterface $normalizer
    ): Response {
        try {
            $product = $productGetter->getById($id);
        } catch (NotFoundHttpException $exception) {
            return $this->json(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }

        return $this->json([
            'product' => $normalizer->normalize($product, 'json', [
                AbstractNormalizer::ATTRIBUTES => [
                    'name',
                    'price',
                    'VAT',
                    'category',
                    'description',
                    'owner',
                    'deliveryTime',
                ],
            ])
        ]);
    }

    #[Route('/products', name: 'get_all_products', methods: ['GET'])]
    public function getAllProducts(
        Request $request, ProductGetter $productGetter, NormalizerInterface $normalizer
    ): Response {
        return $this->json(
            $normalizer->normalize($productGetter->getAll($request->query->get('category')), 'json', [
                AbstractNormalizer::ATTRIBUTES => [
                    'name',
                    'price',
                    'VAT',
                    'category',
                    'description',
                    'owner',
                    'deliveryTime',
                ],
            ])
        );
    }
}
