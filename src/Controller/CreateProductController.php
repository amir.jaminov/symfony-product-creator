<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\ProductDto;
use App\InvalidProductArgumentException;
use App\Service\ProductService;
use Exception;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CreateProductController extends AbstractController
{
    #[Route('/products/create', name: 'create_product', methods: ['POST'])]
    public function createProduct(
        Request $request, ProductService $productService, NormalizerInterface $normalizer
    ): Response {
        try {
            $product = $productService->create($request);
        } catch (InvalidProductArgumentException $exception) {
            return $this->json(['error' => $exception->getViolationMessageList()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (Exception) {
            return $this->json(['error' => 'Internal server error'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json([
            'product' => $normalizer->normalize($product, 'json'),
        ], Response::HTTP_CREATED);
    }
}
