<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\ProductGetter;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductGetterTest extends TestCase
{
    use ProphecyTrait;

    private ProductRepository | ObjectProphecy $productRepository;
    private ProductGetter $productGetter;

    public function setUp(): void
    {
        $this->productRepository = self::prophesize(ProductRepository::class);

        $this->productGetter = new ProductGetter($this->productRepository->reveal());
    }

    public function testGetByIdThrowsExceptionWhenProductNotFound(): void
    {
        $notExistingId = 999;
        $this->productRepository->find($notExistingId)->willReturn(null);

        self::expectException(NotFoundHttpException::class);

        $this->productGetter->getById($notExistingId);
    }

    public function testGetByIdReturnsProduct(): void
    {
        $id = 1;

        $product = new Product();
        $product->setName('Test');

        $this->productRepository->find($id)->willReturn($product);

        $actual = $this->productGetter->getById($id);

        self::assertSame($product, $actual);
    }

    public function testGetAllReturnsAllProductsWhenCategoryNotProvided(): void
    {
        $product = new Product();
        $products = [$product];

        $this->productRepository->findBy(Argument::any())->shouldNotBeCalled();
        $this->productRepository->findAll()->willReturn($products);

        $actual = $this->productGetter->getAll();

        self::assertSame($products, $actual);
    }

    public function testGetAllReturnsProductsFilteredByCategory(): void
    {
        $category = 'category name';
        $product = new Product();
        $products = [$product];

        $this->productRepository->findAll()->shouldNotBeCalled();
        $this->productRepository->findBy(['category' => $category])->willReturn($products);

        $actual = $this->productGetter->getAll($category);

        self::assertSame($products, $actual);
    }
}
