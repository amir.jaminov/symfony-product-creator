<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\DTO\ProductDto;
use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\ProductService;
use App\Service\ProductValidator;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductServiceTest extends TestCase
{
    use ProphecyTrait;

    private ProductRepository | ObjectProphecy $productRepository;
    private ProductValidator | ObjectProphecy $validator;
    private ProductService $productService;

    public function setUp(): void
    {
        $this->productRepository = self::prophesize(ProductRepository::class);
        $this->validator = self::prophesize(ProductValidator::class);

        $this->productService = new ProductService($this->productRepository->reveal(), $this->validator->reveal());
    }

    public function testCreateSaveProductWithVAT20WhenVATNotProvided(): void
    {
        $request = new Request(
            [], ['name' => 'some name', 'category' => 'some category']
        );

        $productDto = new ProductDto();
        $productDto->VAT = 20.0;
        $productDto->name = 'some name';
        $productDto->price = null;
        $productDto->category = 'some category';
        $productDto->description = null;
        $productDto->owner = null;
        $productDto->deliveryTime = null;

        $product = new Product();
        $product->setVAT(20.0);
        $product->setName('some name');
        $product->setCategory('some category');

        $this->validator->validate($productDto, ['create'])->shouldBeCalledOnce();

        $this->productRepository->save($product)->shouldBeCalledOnce();

        $actual = $this->productService->create($request);

        self::assertEquals($product, $actual);
    }

    public function testUpdateThrowsNotFoundExceptionWhenProductNotFound(): void
    {
        $notExistingId = 999;
        $request = new Request();

        $productDto = new ProductDto();
        $productDto->VAT = null;
        $productDto->name = null;
        $productDto->price = null;
        $productDto->category = null;
        $productDto->description = null;
        $productDto->owner = null;
        $productDto->deliveryTime = null;

        $this->validator->validate($productDto)->shouldBeCalledOnce();
        $this->productRepository->save(Argument::any())->shouldNotBeCalled();

        $this->productRepository->find($notExistingId)->willReturn(null);

        self::expectException(NotFoundHttpException::class);

        $this->productService->update($request, $notExistingId);
    }

    public function testUpdateSaveProduct(): void
    {
        $id = 1;
        $request = new Request(
            [], ['VAT' => '20', 'name' => 'some new name', 'category' => 'some new category']
        );

        $productDto = new ProductDto();
        $productDto->VAT = 20.0;
        $productDto->name = 'some new name';
        $productDto->price = null;
        $productDto->category = 'some new category';
        $productDto->description = null;
        $productDto->owner = null;
        $productDto->deliveryTime = null;

        $product = new Product();
        $product->setVAT(20.0);
        $product->setName('some new name');
        $product->setCategory('some new category');

        $this->productRepository->find($id)->willReturn($product);

        $this->validator->validate($productDto)->shouldBeCalledOnce();
        $this->productRepository->save($product)->shouldBeCalledOnce();

        $this->productService->update($request, $id);
    }
}
