<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\DTO\ProductDto;
use App\InvalidProductArgumentException;
use App\Service\ProductValidator;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductValidatorTest extends TestCase
{
    use ProphecyTrait;

    private ValidatorInterface | ObjectProphecy $validator;
    private ConstraintViolationListInterface | ObjectProphecy $constraintViolationList;

    private ProductValidator $productValidator;

    public function setUp(): void
    {
        $this->validator = self::prophesize(ValidatorInterface::class);

        $this->productValidator = new ProductValidator($this->validator->reveal());
    }

    public function testValidateThrowsInvalidProductArgumentException(): void
    {
        $productDto = new ProductDto();
        $groups = ['group'];

        $violation = self::prophesize(ConstraintViolation::class);
        $violation->getPropertyPath()->willReturn('path');
        $violation->getMessageTemplate()->willReturn('message template');
        $violationList = new ConstraintViolationList([$violation->reveal()]);

        $this->validator->validate($productDto, null, $groups)->willReturn($violationList);

        self::expectException(InvalidProductArgumentException::class);

        $this->productValidator->validate($productDto, $groups);
    }

    public function testValidateWillNotThrowExceptions(): void
    {
        $productDto = new ProductDto();
        $groups = ['group'];

        $violationList = self::prophesize(ConstraintViolationList::class);
        $violationList->count()->willReturn(0);

        $this->validator->validate($productDto, null, $groups)->willReturn($violationList);

        $this->productValidator->validate($productDto, $groups);

        self::assertTrue(true);
    }
}
