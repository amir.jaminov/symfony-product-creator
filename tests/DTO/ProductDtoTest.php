<?php

declare(strict_types=1);

namespace App\Tests\DTO;

use App\DTO\ProductDto;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class ProductDtoTest extends TestCase
{
    public function testGetters(): void
    {
        $productDto = new ProductDto();
        $productDto->VAT = 20.0;
        $productDto->name = 'some new name';
        $productDto->price = 11.1;
        $productDto->category = 'some new category';
        $productDto->description = 'desc';
        $productDto->owner = 'owner';
        $productDto->deliveryTime = 100;

        self::assertEquals([
            20.0, 'some new name', 11.1, 'some new category', 'desc', 'owner', 100,
        ], [
            $productDto->getVAT(),
            $productDto->getName(),
            $productDto->getPrice(),
            $productDto->getCategory(),
            $productDto->getDescription(),
            $productDto->getOwner(),
            $productDto->getDeliveryTime(),
        ]);
    }

    public function testCreatorReturnsCorrectDto(): void
    {
        $request = new Request(
            [], ['name' => 'some new name', 'category' => 'some new category']
        );

        $productDto = ProductDto::creator($request);

        $expected = new ProductDto();
        $expected->name = 'some new name';
        $expected->price = null;
        $expected->VAT = null;
        $expected->category = 'some new category';
        $expected->description = null;
        $expected->owner = null;
        $expected->deliveryTime = null;

        self::assertEquals($expected, $productDto);
    }
}
