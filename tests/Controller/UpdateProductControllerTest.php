<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\UpdateProductController;
use App\InvalidProductArgumentException;
use App\Service\ProductService;
use Exception;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class UpdateProductControllerTest extends TestCase
{
    use ProphecyTrait;

    private Request | ObjectProphecy $request;
    private ProductService | ObjectProphecy $productService;
    private NormalizerInterface | ObjectProphecy $normalizer;
    private UpdateProductController $controller;
    private ContainerInterface | ObjectProphecy $container;

    public function setUp(): void
    {
        $this->request = self::prophesize(Request::class);
        $this->productService = self::prophesize(ProductService::class);
        $this->normalizer = self::prophesize(NormalizerInterface::class);
        $this->container = self::prophesize(ContainerInterface::class);

        $this->container->has('serializer')->willReturn(false);

        $this->controller = new UpdateProductController();
        $this->controller->setContainer($this->container->reveal());
    }

    public function testCreateProductReturns422StatusWhenIncorrectDataProvided(): void
    {
        $id = 1;
        $errorMessage = 'error message';

        $this->productService->update($this->request->reveal(), $id)->willThrow(
            new InvalidProductArgumentException([$errorMessage])
        );

        $response = $this->controller->updateProduct(
            $id, $this->request->reveal(), $this->productService->reveal()
        );

        self::assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        self::assertEquals(json_encode(['error' => [$errorMessage]]), $response->getContent());
    }

    public function testCreateProductReturns500WhenUnhandledErrorAppeares(): void
    {
        $id = 1;
        $this->productService->update($this->request->reveal(), $id)->willThrow(
            new Exception()
        );

        $response = $this->controller->updateProduct(
            $id, $this->request->reveal(), $this->productService->reveal()
        );

        self::assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
        self::assertEquals(json_encode(['error' => 'Internal server error']), $response->getContent());
    }

    public function testCreateProductReturns404WhenProductNotFound(): void
    {
        $notExistingId = 999;
        $errorMessage = 'Product not found';

        $this->productService->update($this->request->reveal(), $notExistingId)->willThrow(
            new NotFoundHttpException('Product not found')
        );

        $response = $this->controller->updateProduct(
            $notExistingId, $this->request->reveal(), $this->productService->reveal()
        );

        self::assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        self::assertEquals(json_encode(['error' => $errorMessage]), $response->getContent());
    }

    public function testCreateProductReturnsEmptyResponseWhenProductUpdated(): void
    {
        $id = 1;
        $this->productService->update($this->request->reveal(), $id)->shouldBeCalledOnce();

        $response = $this->controller->updateProduct(
            $id, $this->request->reveal(), $this->productService->reveal()
        );

        self::assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
        self::assertEquals('{}', $response->getContent());
    }
}
