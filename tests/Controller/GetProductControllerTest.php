<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\GetProductController;
use App\Entity\Product;
use App\Service\ProductGetter;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class GetProductControllerTest extends TestCase
{
    use ProphecyTrait;

    private Request | ObjectProphecy $request;
    private ProductGetter | ObjectProphecy $productGetter;
    private NormalizerInterface | ObjectProphecy $normalizer;
    private GetProductController $controller;
    private ContainerInterface | ObjectProphecy $container;

    public function setUp(): void
    {
        $this->request = self::prophesize(Request::class);
        $this->productGetter = self::prophesize(ProductGetter::class);
        $this->normalizer = self::prophesize(NormalizerInterface::class);
        $this->container = self::prophesize(ContainerInterface::class);

        $this->container->has('serializer')->willReturn(false);

        $this->controller = new GetProductController();
        $this->controller->setContainer($this->container->reveal());
    }

    public function testGetProductReturns404WhenProductNotFound(): void
    {
        $notExistingId = 999;
        $errorMessage = 'Product not found';

        $this->productGetter->getById($notExistingId)->willThrow(
            new NotFoundHttpException($errorMessage)
        );

        $response = $this->controller->getProduct(
            $notExistingId, $this->productGetter->reveal(), $this->normalizer->reveal()
        );

        self::assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        self::assertEquals(json_encode(['error' => $errorMessage]), $response->getContent());
    }

    public function testGetProductReturnsProductByProvidedId(): void
    {
        $id = 1;
        $product = new Product();
        $product->setName('Name');

        $normalizedProduct = [
            'name' => $product->getName(),
        ];

        $this->normalizer->normalize($product, 'json', [
            AbstractNormalizer::ATTRIBUTES => [
                'name',
                'price',
                'category',
            ],
        ])->willReturn($normalizedProduct);

        $this->productGetter->getById($id)->willReturn($product);

        $response = $this->controller->getProduct(
            $id, $this->productGetter->reveal(), $this->normalizer->reveal()
        );

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
        self::assertEquals(json_encode(['product' => $normalizedProduct]), $response->getContent());
    }

    public function testGetFullProductWillReturn404WhenProductNotFound(): void
    {
        $notExistingId = 999;
        $errorMessage = 'Product not found';

        $this->productGetter->getById($notExistingId)->willThrow(
            new NotFoundHttpException($errorMessage)
        );

        $response = $this->controller->getFullProduct(
            $notExistingId, $this->productGetter->reveal(), $this->normalizer->reveal()
        );

        self::assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        self::assertEquals(json_encode(['error' => $errorMessage]), $response->getContent());
    }

    public function testGetFullProductReturnsProductByProvidedId(): void
    {
        $id = 1;
        $product = new Product();
        $product->setName('Name');

        $normalizedProduct = [
            'name' => $product->getName(),
        ];

        $this->normalizer->normalize($product, 'json', [
            AbstractNormalizer::ATTRIBUTES => [
                'name',
                'price',
                'VAT',
                'category',
                'description',
                'owner',
                'deliveryTime',
            ],
        ])->willReturn($normalizedProduct);

        $this->productGetter->getById($id)->willReturn($product);

        $response = $this->controller->getFullProduct(
            $id, $this->productGetter->reveal(), $this->normalizer->reveal()
        );

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
        self::assertEquals(json_encode(['product' => $normalizedProduct]), $response->getContent());
    }

    public function testGetAllProductsReturnsProducts(): void
    {
        $product = new Product();
        $product->setName('Name');
        $request = new Request(['category' => 'some category']);

        $normalizedProducts = [
            [
                'name' => $product->getName(),
            ],
        ];

        $this->productGetter->getAll('some category')->willReturn([$product]);

        $this->normalizer->normalize([$product], 'json', [
            AbstractNormalizer::ATTRIBUTES => [
                'name',
                'price',
                'VAT',
                'category',
                'description',
                'owner',
                'deliveryTime',
            ],
        ])->willReturn($normalizedProducts);

        $response = $this->controller->getAllProducts(
            $request, $this->productGetter->reveal(), $this->normalizer->reveal()
        );

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
        self::assertEquals(json_encode($normalizedProducts), $response->getContent());
    }
}
