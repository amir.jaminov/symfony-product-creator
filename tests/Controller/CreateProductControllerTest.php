<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\CreateProductController;
use App\Entity\Product;
use App\InvalidProductArgumentException;
use App\Service\ProductService;
use Exception;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CreateProductControllerTest extends TestCase
{
    use ProphecyTrait;

    private Request | ObjectProphecy $request;
    private ProductService | ObjectProphecy $productService;
    private NormalizerInterface | ObjectProphecy $normalizer;
    private CreateProductController $controller;
    private ContainerInterface | ObjectProphecy $container;

    public function setUp(): void
    {
        $this->request = self::prophesize(Request::class);
        $this->productService = self::prophesize(ProductService::class);
        $this->normalizer = self::prophesize(NormalizerInterface::class);
        $this->container = self::prophesize(ContainerInterface::class);

        $this->container->has('serializer')->willReturn(false);

        $this->controller = new CreateProductController();
        $this->controller->setContainer($this->container->reveal());
    }

    public function testCreateProductReturns422StatusWhenIncorrectDataProvided(): void
    {
        $errorMessage = 'error message';

        $this->productService->create($this->request->reveal())->willThrow(
            new InvalidProductArgumentException([$errorMessage])
        );

        $response = $this->controller->createProduct(
            $this->request->reveal(),
            $this->productService->reveal(),
            $this->normalizer->reveal()
        );

        self::assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        self::assertEquals(json_encode(['error' => [$errorMessage]]), $response->getContent());
    }

    public function testCreateProductReturns500WhenUnhandledErrorAppeares(): void
    {
        $this->productService->create($this->request->reveal())->willThrow(
            new Exception()
        );

        $response = $this->controller->createProduct(
            $this->request->reveal(),
            $this->productService->reveal(),
            $this->normalizer->reveal()
        );

        self::assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
        self::assertEquals(json_encode(['error' => 'Internal server error']), $response->getContent());
    }

    public function testCreateProductReturnsCreatedProduct(): void
    {
        $product = new Product();
        $product->setName('Test');

        $normalizedProduct = [
            'name' => $product->getName(),
        ];

        $this->productService->create($this->request->reveal())->willReturn($product);
        $this->normalizer->normalize($product, 'json')->willReturn($normalizedProduct);

        $response = $this->controller->createProduct(
            $this->request->reveal(),
            $this->productService->reveal(),
            $this->normalizer->reveal()
        );

        self::assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        self::assertEquals(json_encode(['product' => $normalizedProduct]), $response->getContent());
    }
}
