# the different stages of this Dockerfile are meant to be built into separate images
# https://docs.docker.com/develop/develop-images/multistage-build/#stop-at-a-specific-build-stage
# https://docs.docker.com/compose/compose-file/#target


# https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact
#ARG PHP_VERSION=8.1.5
#ARG NGINX_VERSION=1.20
#ARG NODE_VERSION=18.1.0
#
## "php" stage
FROM php:8.1.5-fpm-alpine3.15 AS php

RUN set -eux; \
	apk add --no-cache --virtual .build-deps \
#		$PHPIZE_DEPS \
		icu-dev \
		libzip-dev \
		zlib-dev \
		mysql-dev \
		freetype \
		freetype-dev \
	; \
	docker-php-ext-configure zip; \
	docker-php-ext-configure gd --with-freetype; \
	docker-php-ext-install -j$(nproc) \
		intl \
		zip \
		gd \
		pdo_mysql \
	;
#	pecl install \
#		apcu-${APCU_VERSION} \
#		pcov \
#	; \
#	pecl clear-cache; \
#	docker-php-ext-enable \
#		apcu \
#		opcache \
#		gd \
#	; \
#	runDeps="$( \
#		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
#			| tr ',' '\n' \
#			| sort -u \
#			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
#	)"; \
#	apk add --no-cache --virtual .phpexts-rundeps $runDeps; \
#	apk del .build-deps; \
#	rm -rf /tmp/*

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

ENV COMPOSER_ALLOW_SUPERUSER=1

COPY composer.json composer.lock ./

RUN set -eux; \
	composer install --prefer-dist --no-dev --no-scripts --no-progress; \
	composer clear-cache;

COPY docker/php/docker-entrypoint.sh /usr/local/bin/docker-entrypoint

ENTRYPOINT ["docker-entrypoint"]
CMD ["php-fpm"]

####################
# "nginx" stage
# depends on the "php" stage above
####################

FROM nginx:latest AS nginx

COPY docker/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

#WORKDIR /srv/app/public

#COPY --from=php /srv/app/public ./
